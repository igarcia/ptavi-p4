#!/usr/bin/python3
"""  UDP CLIENT PROGRAM """

import socket
import sys

""" read arguments """
try:
    if len(sys.argv) > 6:
        sys.exit("Ussage client.py ip port register sip_address expires_value")
    else:
        server_ip = sys.argv[1]
        server_port = int(sys.argv[2])
        register = sys.argv[3]
        address = sys.argv[4]
        expires = sys.argv[5]
except IndexError:
    sys.exit("Ussage client.py ip port register sip_address expires_value")

""" create socket """
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    """ conect socket to a server """
    my_socket.connect((server_ip, server_port))

    """ the message we send to the server"""
    message = ""
    sip_request = "REGISTER sip:" + str(address) + " SIP/2.0\r\n\r\n"
    expires_message = "Expires: " + expires + ("\r\n\r\n")
    print("Enviando:", sip_request)
    print("Enviando:", expires_message)
    message = sip_request + expires_message
    my_socket.send(bytes(message, 'utf-8'))

    """ the message we read from the server"""
    data = my_socket.recv(1024)
    print('----- Recibido ----- \r\n ')
    print(data.decode('utf-8'))

print("Socket terminado.")
