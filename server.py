#!/usr/bin/python3
""" UDP ECHO SERVER IN SIMPLE """

import socketserver
import sys
import time
import json
import os.path as path

""" register server class """


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ variables """
    user_dicc = {}

    """ handle method of the server class
    (all requests will be handled by this method) """
    def handle(self):
        self.wfile.write(b"Hemos recibido tu peticion\r\n\r\n")
        lines_bit = self.rfile.read()
        lines_decod = lines_bit.decode('utf8')
        lines_split = lines_decod.split(" ")

        """ messages REGISTER """
        if lines_split[0] == "REGISTER":
            """ we collect the data that interest us for the client """
            client_address = lines_split[1].split("sip:")[1]
            client_ip = self.client_address[0]
            format = "%Y-%m-%d %H:%M:%S"
            expires_time = time.time() + int(lines_split[3])
            expires = time.strftime(format, time.localtime(expires_time))
            client_data = [client_ip, expires]

            """ we save the data or delete it according to the expire """
            self.json2register()
            self.user_dicc[client_address] = client_data
            self.registrar()
            if int(lines_split[3]) == 0:
                del self.user_dicc[client_address]
            print(self.user_dicc)
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

        self.register2json()

        """ we write the client's ip and the client's port """
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        print("client_ip:" + str(client_ip))
        print("client_port:" + str(client_port))

    """ check if a client has expired and delete it """
    def registrar(self):
        usser_dicc_2 = []

        format = "%Y-%m-%d %H:%M:%S"
        time_now = time.strftime(format, time.localtime())
        for user in self.user_dicc:
            if self.user_dicc[user][1] < time_now:
                usser_dicc_2.append(user)
        for user in usser_dicc_2:
            del self.user_dicc[user]

    """ write the collection of clients to the file """
    def register2json(self):
        with open("registered.json", "w") as file:
            json.dump(self.user_dicc, file, indent=1)

    """check if registered.json exists."""
    def json2register(self):
        try:
            if path.exists("registered.json"):
                with open("registered.json", "r") as data_file:
                    self.user_dicc[username] = json.load(data_file)
        except Exception:
            pass

if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    port = int(sys.argv[1])
    serv = socketserver.UDPServer(('', port), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...\r\n\r\n")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
